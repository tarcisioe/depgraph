BASEDIR=$(dirname $0)

sphinx-apidoc -o doc_build "$BASEDIR"/../depgraph -F -H 'depgraph' -A 'Tarcísio Eduardo Moreira Crocomo, Arthur Bridi Guazzelli'
cp "$BASEDIR"/conf.py doc_build
cd doc_build
make html
