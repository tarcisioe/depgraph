from collections import namedtuple
from enum import Enum

from depgraph.digraph import Digraph


class TaskState(Enum):
    DONE = 0
    READY = 1
    NOT_READY = 2


class TaskType(Enum):
    REGULAR = 0
    EXTERNAL = 1
    FINAL = 2


class Task(namedtuple('Task', 'name done task_type')):
    pass


def make_task(name_or_task, done=False, task_type=TaskType.REGULAR):
    if isinstance(name_or_task, Task):
        return name_or_task
    return Task(name_or_task, done, task_type)


class TaskGraph:
    def __init__(self, name='Tasks'):
        self.name = name
        self.d = Digraph()

    def insert_task(self, task):
        self.d.insert(make_task(task))

    def add_dependency(self, needed, dependent):
        self.d.link(dependent, needed)

    def dependencies(self, task):
        return self.d.reachable(task) - {task}

    def direct_dependencies(self, task):
        return self.d.vertices[task].neighbors

    def as_dict(self):
        return self.d.reversed().vertices

    def set_done(self, task, done=True):
        if done and self.state(task) == TaskState.NOT_READY:
            raise ValueError("Cannot set a task as done if it is not ready.")

        (name, _, task_type) = self.d.vertex(task)
        self.d.update(task, Task(name, done, task_type))

    def state(self, task):
        if self.d.vertex(task).done:
            return TaskState.DONE

        if all(self.d.vertex(w).done for w in self.d.neighbors(task)):
            return TaskState.READY

        return TaskState.NOT_READY

    def states(self):
        return {v: self.state(v) for v in self.d.vertices}

    def task_types(self):
        return {v: self.d.vertex(v).task_type for v in self.d.vertices}
