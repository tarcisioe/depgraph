from collections import deque, namedtuple


class Vertex(namedtuple('Vertex', 'obj neighbors')):
    """Representation of a Vertex

    Args:
        obj, neighbors (tuple): the object that the vertex will hold and it's
        neighboring vertices, respectively.
    """
    pass


class Digraph:
    """Representation of a Digraph"""

    def __init__(self):
        """Constructor for Digraph"""
        self.vertices = {}

    def insert(self, vertex, name=None):
        """Insert a vertex into the digraph.

        Args:
            vertex (Object): the object to insert as a vertex
            name (str): a name to use if vertex has no `name` attribute
        """
        if name is None:
            try:
                name = vertex.name
            except AttributeError:
                name = vertex

        if name in self.vertices:
            return
        self.vertices[name] = Vertex(vertex, set())

    def update(self, v, new):
        """Update a vertex on the digraph.

        Args:
            v (str): the name of the vertex to update.
            new (Object): a new object to attribute to the vertex.
        """
        _, neighbors = self.vertices[v]
        self.vertices[v] = Vertex(new, neighbors)

    def vertex(self, v):
        """Gets the object from a vertex.

        Args:
            v (str): the name of the vertex that contains the object.

        Returns:
            object: the vertex's object.
        """
        return self.vertices[v].obj

    def neighbors(self, v):
        """Gets the neighbors of a vertex.

        Args:
            v (str): the name of the vertex to get the neighbors from.

        Returns:
            set: the neighbors of vertex v
        """
        return self.vertices[v].neighbors

    def link(self, v1, v2):
        """Links two vertices in a digraph fashion (v1 -> v2).

        Args:
            v1 (str): the name of the vertex to have a neighbor added.
            v2 (str): the name of the vertex to be added as a neighbor.
        """
        if v2 not in self.vertices:
            raise IndexError('{} is not a vertex'.format(v2))
        self.vertices[v1].neighbors.add(v2)

    def reversed(self):
        """Makes a new, reversed digraph out of the current one.

        Returns:
            digraph: the reversed digraph.
        """
        inverted = Digraph()
        for v, (obj, ws) in self.vertices.items():
            inverted.insert(obj, v)
            for w in ws:
                inverted.insert(self.vertices[w].obj, w)
                inverted.link(w, v)
        return inverted

    def simple_bfs(self, v):
        """Perform a simple breadth-first search starting from v.

        Args:
            v (str): the identifier for the vertex to start from.

        Returns:
            list: an ordering of vertices in bfs form from v.
        """
        queue = deque([v])
        visited = set()
        result = []

        while queue:
            w = queue.popleft()
            visited.add(w)
            result.append(w)
            queue.extend(self.vertices[w].neighbors)

        return result

    def reachable(self, v):
        """Looks for vertices that are reachable from v.

        Args:
            v (str): the identifier for the vertex to start from.

        Returns:
            set: all vertices that have a path from v to that vertice.
            Essentially the result of a bfs starting from v given as a set.
        """
        return set(self.simple_bfs(v))
