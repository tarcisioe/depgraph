from pytest import fixture, raises

from depgraph.taskgraph import Task, TaskGraph, TaskState, TaskType, make_task


@fixture
def tasks():
    tasks = TaskGraph()
    tasks.insert_task("Do A")
    tasks.insert_task("Do B with A")
    tasks.add_dependency("Do A", "Do B with A")
    tasks.insert_task("Do C with B")
    tasks.add_dependency("Do B with A", "Do C with B")
    return tasks


def test_make_task_from_string():
    task = make_task("Task")
    assert task == Task("Task", done=False, task_type=TaskType.REGULAR)


def test_make_task_from_instance():
    instance = Task("Task", done=True, task_type=TaskType.FINAL)
    task = make_task(instance)
    assert task == instance


def test_dependencies(tasks):
    assert tasks.dependencies("Do C with B") == {"Do A", "Do B with A"}


def test_direct_dependencies(tasks):
    assert tasks.direct_dependencies("Do C with B") == {"Do B with A"}


def test_states(tasks):
    states = tasks.states()
    assert states["Do C with B"] == TaskState.NOT_READY
    assert states["Do B with A"] == TaskState.NOT_READY
    assert states["Do A"] == TaskState.READY


def test_states_with_done(tasks):
    tasks.set_done("Do A")
    states = tasks.states()
    assert states["Do C with B"] == TaskState.NOT_READY
    assert states["Do B with A"] == TaskState.READY
    assert states["Do A"] == TaskState.DONE


def test_cannot_set_done_not_ready(tasks):
    with raises(ValueError):
        tasks.set_done("Do B with A")
