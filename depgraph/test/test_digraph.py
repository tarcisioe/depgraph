from pytest import raises, fixture

from depgraph.digraph import Digraph


@fixture
def digraph():
    d = Digraph()
    return d


def test_can_insert(digraph):
    digraph.insert('1')
    assert '1' in digraph.vertices


def test_can_link(digraph):
    digraph.insert('1')
    digraph.insert('2')
    digraph.link('1', '2')
    assert '2' in digraph.neighbors('1')


def test_cannot_link_nonexistent(digraph):
    digraph.insert('1')
    with raises(IndexError):
        digraph.link('1', '2')


def test_invert_linear_digraph(digraph):
    digraph.insert('1')
    digraph.insert('2')
    digraph.insert('3')
    digraph.link('1', '2')
    digraph.link('2', '3')
    r = digraph.reversed()
    assert r.neighbors('3') == {'2'}
    assert r.neighbors('2') == {'1'}
    assert r.neighbors('1') == set()


def test_invert_forked_digraph(digraph):
    digraph.insert('1')
    digraph.insert('2')
    digraph.insert('3')
    digraph.insert('4')
    digraph.link('1', '2')
    digraph.link('2', '3')
    digraph.link('2', '4')
    r = digraph.reversed()
    assert r.neighbors('4') == {'2'}
    assert r.neighbors('3') == {'2'}
    assert r.neighbors('2') == {'1'}
    assert r.neighbors('1') == set()
