from textwrap import dedent


from depgraph.taskgraph import TaskGraph
from depgraph.dot import make_dot


EXPECTED_DOT = {
    'Vina': dedent('''\
            digraph Vina {
            {
            node [style=filled]
            "Camisetas" [fillcolor="#d73027" shape="ellipse"]
            "Logo" [fillcolor="#fdae61" shape="ellipse"]
            }
            "Camisetas" -> {}
            "Logo" -> {"Camisetas"}
            }''')
}


def test_make_dot():
    g = TaskGraph('Vina')
    g.insert_task('Camisetas')
    g.insert_task('Logo')

    g.add_dependency('Logo', 'Camisetas')

    assert make_dot(g) == EXPECTED_DOT['Vina']
