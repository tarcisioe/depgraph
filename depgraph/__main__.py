from depgraph.taskgraph import TaskGraph, TaskType, make_task
from depgraph.dot import make_dot

def main():
    vina = TaskGraph()

    budget = "Fazer orçamento"
    logo = "Ter todos os logotipos"
    draw_tees = "Desenhar a camiseta"
    make_tees = "Enviar modelo para a malharia"
    colors = "Definir cores"
    layout = "Definir layout"
    staff_tees = "Camiseta staff"
    style = "Definir estilo da arte"
    sizes = "Definir quantidade de tamanhos"
    contacts = "Contatos"

    vina.insert_task(budget)
    vina.insert_task(logo)
    vina.insert_task(draw_tees)
    vina.insert_task(make_tees)
    vina.insert_task(colors)
    vina.insert_task(layout)
    vina.insert_task(make_task(staff_tees, task_type=TaskType.FINAL))
    vina.insert_task(style)
    vina.insert_task(sizes)
    vina.insert_task(make_task(contacts, task_type=TaskType.EXTERNAL))

    vina.add_dependency(make_tees, staff_tees)
    vina.add_dependency(budget, make_tees)
    vina.add_dependency(draw_tees, make_tees)
    vina.add_dependency(sizes, budget)
    vina.add_dependency(colors, budget)
    vina.add_dependency(layout, draw_tees)
    vina.add_dependency(logo, draw_tees)
    vina.add_dependency(style, colors)
    vina.add_dependency(style, layout)
    vina.add_dependency(contacts, logo)

    vina.set_done(contacts)

    print(make_dot(vina))


if __name__ == '__main__':
    main()
