from depgraph.taskgraph import TaskState, TaskType

COLORS = {
    TaskState.DONE: '#66bd63',
    TaskState.READY: '#fdae61',
    TaskState.NOT_READY: '#d73027',
}

SHAPES = {
    TaskType.REGULAR: 'ellipse',
    TaskType.EXTERNAL: 'rectangle',
    TaskType.FINAL: 'diamond',
}


def styles_from_tasks(digraph):
    states = digraph.states()
    task_types = digraph.task_types()

    colors = ['fillcolor="{}"'.format(COLORS[states[s]])
              for s in sorted(states)]
    shapes = ['shape="{}"'.format(SHAPES[task_types[t]])
              for t in sorted(task_types)]
    return ['"{}" [{} {}]'.format(v, c, s)
            for v, c, s in zip(sorted(states.keys()), colors, shapes)]


def make_dot(digraph):
    dot_lines = []
    dot_lines.append('digraph {} {{'.format(digraph.name))

    dot_lines.extend([
        '{',
        'node [style=filled]',
    ])

    dot_lines.extend(styles_from_tasks(digraph))

    dot_lines.append('}')

    for v, (_, ws) in sorted(digraph.as_dict().items()):
        neighbors = ' '.join('"{}"'.format(w) for w in sorted(ws))
        dot_lines.append('"{}" -> {{{}}}'.format(v, neighbors))

    dot_lines.append('}')
    return '\n'.join(dot_lines)
