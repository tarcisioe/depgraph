from setuptools import setup, find_packages

setup(
    name='depgraph',
    packages=find_packages(),
    install_requires=[
        'pydotplus',
    ]
)
